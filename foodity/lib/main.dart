import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:foodity/src/services/authentication.dart';
import 'package:foodity/src/models/root_page.dart';

void main() {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(new MyApp());
  });
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        theme: ThemeData(primaryColor: Colors.amber[600]),
        debugShowCheckedModeBanner: false,
        home: new RootPage(auth: new Auth()));
  }
}

// void main() {

// }

// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: MyHomePage(),
//     );
//   }
// }

















