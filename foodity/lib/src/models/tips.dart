import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:foodity/src/models/colors.dart';

class Tips extends StatefulWidget {
  _Tips createState() => _Tips();
}

class _Tips extends State<Tips> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            'Tips',
            style: textS,
          ),
          leading: IconButton(
            icon: Icon(Icons.keyboard_backspace),
            onPressed: () {
              Navigator.pop(context);
            },
            color: Colors.white,
          ),
          backgroundColor: naranja,
        ),
        body: StreamBuilder(
            stream: Firestore.instance.collection("tips").snapshots(),
            builder: (context, snapshot) {
              if (!snapshot.hasData) return const Text('Cargando..');
              return ListView.builder(
                itemCount: snapshot.data.documents.length,
                itemBuilder: (context, index) =>
                    _buildListItem(context, snapshot.data.documents[index]),
              );
            }));
  }

  Widget _buildListItem(BuildContext context, document) {
    String descripcion = document['descripcion'];

    return Card(
        child: Container(
          padding: EdgeInsets.all(10),
      child: Text(descripcion,
          style: new TextStyle(color: Colors.white, fontSize: 25)),
      color: verde,
    ));
  }
}
