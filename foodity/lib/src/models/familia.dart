import 'package:flutter/material.dart';

class Familia extends StatefulWidget {
  _Familia createState() => _Familia();
}

class _Familia extends State<Familia> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(children: <Widget>[
      Text("Mi familia va a estar aquí"),
      FlatButton(
        onPressed: () {
          Navigator.pop(context);
        },
        child: Text('Volver'),
      )
    ]));
  }
}