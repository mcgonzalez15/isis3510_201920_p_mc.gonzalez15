import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foodity/src/models/colors.dart';
import 'package:flip_card/flip_card.dart';

class Recetas extends StatefulWidget {
  _Recetas createState() => _Recetas();
}

class _Recetas extends State<Recetas> {
  @override
  void initState() {
    super.initState();
  }

  List<Receta> recetas = new List<Receta>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            'Recetas',
            style: textS,
          ),
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () {
              Navigator.pop(context);
            },
            color: Colors.white,
          ),
          backgroundColor: rojo,
        ),
        body: StreamBuilder(
            stream: Firestore.instance.collection("recetas").snapshots(),
            builder: (context, snapshot) {
              if (!snapshot.hasData) return const Text('Cargando..');
              return GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2),
                itemCount: snapshot.data.documents.length,
                itemBuilder: (context, index) => _buildListItem(
                    context, snapshot.data.documents[index], index),
              );
            }));
  }

  Widget _buildListItem(
      BuildContext context, DocumentSnapshot document, int index) {
    String nombre = document['nombre'];
    String imagen = document['imagen'];
    String dificultad = document['dificultad'];
    Color color = Colors.white;
    List<String> lista = document['ingredientes'].toString().split(',');
    List<String> ingredientes = new List<String>();
    List<String> cantidades = new List<String>();
    List<String> instrucciones = new List<String>();

    for (int i = 0; i < lista.length; i++) {
      if (i % 2 == 0) {
        cantidades.add(lista[i].split('cantidad:')[1]);
      } else
        ingredientes.add(lista[i].replaceRange(0, 8, '').split('}')[0].trim());
    }

    List actual = document['instrucciones'];
    for (int i = 0; i < actual.length; i++) {
      instrucciones.add(actual[i]);
    }

    if (dificultad == 'baja') {
      color = verde;
    } else if (dificultad == 'media') {
      color = amarillo;
    } else
      color = rojo;

    recetas.add(
        new Receta(nombre, ingredientes, instrucciones, imagen, color, cantidades));

    return Center(
      child: Container(
        child: FlipCard(
          flipOnTouch: true,
          front: Card(
            elevation: 3.0,
            color: color,
            child: Stack(
              children: <Widget>[
                Image.network(imagen),
                new Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                      height: 50,
                      alignment: Alignment.center,
                      color: color,
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              child: Text(
                            nombre,
                            style: new TextStyle(color: Colors.white),
                          )),
                          Align(
                            child: IconButton(
                              icon: Icon(Icons.add),
                              color: Colors.white,
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => DetailScreen(
                                      receta: recetas[index],
                                    ),
                                  ),
                                );
                              },
                            ),
                          )
                        ],
                      )),
                )
              ],
            ),
          ),
          back: Container(
              height: 300,
              width: 300,
              child: Card(
                  elevation: 3.0,
                  color: color,
                  child: new ListView.builder(
                    itemCount: ingredientes.length,
                    itemBuilder: (context, index) => _itemIngredientes(
                        context, ingredientes[index], cantidades[index]),
                  ))),
        ),
      ),
    );
  }

  Widget _itemIngredientes(
      BuildContext context, String ingredient, String cantidad) {
    return Container(
      child: Column(
        children: <Widget>[
          Padding(
            child: Container(
              child: Text(
                ingredient,
                style: textS,
              ),
              alignment: Alignment.centerLeft,
            ),
            padding: EdgeInsets.only(left: 5),
          ),
          Align(
            child: Text(
              cantidad,
              style: TextStyle(color: Colors.blueGrey),
            ),
            alignment: Alignment.centerLeft,
          )
        ],
      ),
    );
  }
}

class DetailScreen extends StatelessWidget {
  final Receta receta;

  DetailScreen({Key key, @required this.receta}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.pop(context);
          },
          color: Colors.white,
        ),
        backgroundColor: receta.color,
        title: Text(
          receta.nombre,
          style: textS,
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.star_border),
            onPressed: null,
            color: Colors.white,
          )
        ],
      ),
      body: new ListView.builder(
        itemCount: receta.ingredientes.length + 2 + receta.instrucciones.length,
        itemBuilder: (context, index) => _ingredientes(
            context,
            receta.ingredientes,
            index,
            receta.cantidades,
            receta.instrucciones),
      ),
    );
  }

  Widget _ingredientes(BuildContext context, List<String> ingredient, int index,
      List<String> cantidades, List<String> instrucciones) {
    if (index == 0) {
      return new Align(
        child: Card(
          child: Container(
            child: Text(
              'Ingredientes',
              style: TextStyle(color: Colors.white, fontSize: 18),
            ),
            width: 130,
            height: 30,
            alignment: Alignment.center,
          ),
          elevation: 3.0,
          color: receta.color,
          margin: EdgeInsets.all(8),
        ),
        alignment: Alignment.centerLeft,
      );
    } else if (index == ingredient.length + 1) {
      return new Align(
        child: Card(
          child: Container(
            child: Text(
              'Instrucciones',
              style: TextStyle(color: Colors.white, fontSize: 18),
            ),
            width: 130,
            height: 30,
            alignment: Alignment.center,
          ),
          elevation: 3.0,
          color: receta.color,
          margin: EdgeInsets.all(8),
        ),
        alignment: Alignment.centerLeft,
      );
    } else if (index <= ingredient.length) {
      return new Align(
        child: Card(
          child: Container(
            child: Row(
              children: <Widget>[
                Text(
                  ingredient[index - 1],
                  style: TextStyle(fontSize: 17),
                ),
                Text(
                  cantidades[index - 1],
                  style: TextStyle(color: Colors.grey),
                )
              ],
            ),
            height: 40,
            padding: EdgeInsets.only(left: 10),
          ),
          margin: EdgeInsets.all(3),
        ),
        alignment: Alignment.centerLeft,
      );
    } else {
      int x = (index - 2 - ingredient.length);
      return new Align(
        child: Card(
          child: Container(
            child: Text(
              (x + 1).toString() + '. ' + instrucciones[x],
              style: TextStyle(fontSize: 17),
            ),
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.only(left: 2),
          ),
          margin: EdgeInsets.all(3),
        ),
        
      );
  }}
}

class Receta {
  final String nombre;
  final List<String> ingredientes;
  final List<String> instrucciones;
  final List<String> cantidades;
  final String imagen;
  final Color color;

  Receta(this.nombre, this.ingredientes, this.instrucciones, this.imagen,
      this.color, this.cantidades);
}
