import 'dart:async';
import 'dart:convert';
import 'package:foodity/src/data/error.dart';
import 'package:flutter/material.dart';
import 'package:foodity/src/data/place_response.dart';
import 'package:foodity/src/data/result.dart';
import 'package:foodity/src/models/colors.dart';
import 'package:http/http.dart' as http;
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class PlacesSearchMapSample extends StatefulWidget {
  @override
  State<PlacesSearchMapSample> createState() {
    return _PlacesSearchMapSample();
  }
}

class _PlacesSearchMapSample extends State<PlacesSearchMapSample> {
  static const String _API_KEY = 'AIzaSyBLNMsyDSRVtYLGkVmQApKfSpkbuKzYxFg';

  Map<String, double> currentLocation = Map();
  StreamSubscription<Map<String, double>> streamSubscription;
  Location location = Location();
  
  static double latitude = 4.6014864;
  static double longitude = -74.0663881;
  static const String baseUrl =
      "https://maps.googleapis.com/maps/api/place/nearbysearch/json";

  List<Marker> markers = <Marker>[];
  Error error;
  List<Result> places;
  bool searching = true;
  String keyword;



  static final CameraPosition _myLocation = CameraPosition(
      target: LatLng(latitude, longitude), zoom: 15, bearing: 15.0, tilt: 75.0);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: GoogleMap(
          mapType: MapType.normal,
          initialCameraPosition: _myLocation,
          myLocationEnabled: true,
          scrollGesturesEnabled: false,
          
          markers: Set<Marker>.of(markers),
        ),
        floatingActionButton: new Container(
          alignment: Alignment.bottomLeft,
          padding: EdgeInsets.only(left: 25, bottom: 25),
          child: FloatingActionButton.extended(
            focusColor: rojo,
            backgroundColor: rojo,
            onPressed: () {
              searchNearby(latitude, longitude);
            },
            label: Text('Encontrar Supermercados'),
            icon: Icon(Icons.place),
          ),
        ));
  }

  void _setStyle(GoogleMapController controller) async {
    String value = await DefaultAssetBundle.of(context)
        .loadString('assets/maps_style.json');
    controller.setMapStyle(value);
  }

  void searchNearby(double latitude, double longitude) async {
    setState(() {
      markers.clear();
    });
    String url =
        '$baseUrl?key=$_API_KEY&location=$latitude,$longitude&radius=10000&keyword=supermercado';
    print(url);
    final response = await http.get(url);

    if (response.statusCode == 200) {
      final data = json.decode(response.body);
      _handleResponse(data);
    } else {
      throw Exception('Ocurrió un error.');
    }

    // make sure to hide searching
    setState(() {
      searching = false;
    });
  }

  updateLocation()
  {
    
  }

  void _handleResponse(data) {
    // bad api key or otherwise
    if (data['status'] == "REQUEST_DENIED") {
      setState(() {
        error = Error.fromJson(data);
      });
      
    } else if (data['status'] == "OK") {
      setState(() {
        places = PlaceResponse.parseResults(data['results']);
        for (int i = 0; i < places.length; i++) {
          markers.add(
            Marker(
              markerId: MarkerId(places[i].placeId),
              position: LatLng(places[i].geometry.location.lat,
                  places[i].geometry.location.long),
              infoWindow: InfoWindow(
                  title: places[i].name, snippet: places[i].vicinity),
              onTap: () {},
            ),
          );
        }
      });
    } else {
      print(data);
    }
  }
}
