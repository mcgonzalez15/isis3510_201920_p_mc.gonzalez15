

import 'package:flutter/material.dart';

const rutaImg = "./lib/src/resources/";
const verde = Color.fromRGBO(169, 191, 4, 100);
const amarillo = Color.fromRGBO(242, 183, 5, 100);
const naranja = Color.fromRGBO(242, 159, 5, 100);
const rojo = Color.fromRGBO(217, 4, 4, 100);
const blanco = Color.fromRGBO(242, 242, 242, 100);
const textS = TextStyle(color: Colors.white);