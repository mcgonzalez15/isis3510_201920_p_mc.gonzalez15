import 'package:flutter/material.dart';
import 'package:foodity/src/models/colors.dart';
import 'package:foodity/src/services/authentication.dart';
import 'package:foodity/src/models/mercado.dart';
import 'package:foodity/src/models/familia.dart';
import 'package:foodity/src/models/perfil.dart';
import 'package:foodity/src/models/tips.dart';
import 'package:foodity/src/models/recetas.dart';
import 'package:foodity/src/models/supermercados.dart';
import 'package:foodity/src/models/porciones.dart';
import 'package:foodity/src/models/escribenos.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.auth, this.userId, this.logoutCallback})
      : super(key: key);

  final BaseAuth auth;
  final VoidCallback logoutCallback;
  final String userId;

  @override
  State<StatefulWidget> createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  signOut() async {
    try {
      await widget.auth.signOut();
      widget.logoutCallback();
    } catch (e) {
      print(e);
    }
  }

  int _selectedIndex = 1;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  void _onItemTapped(int index) {
    switch (index) {
      case 0:
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => Mercado()),
        );
        break;
      case 1:
      
        break;
      case 2:
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => Perfil()),
        );
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: naranja,
        title: new Text('Inicio', style: TextStyle(color: Colors.white)),
        actions: <Widget>[
          new FlatButton(
              child: new Text('Logout',
                  style: new TextStyle(fontSize: 17.0, color: Colors.white)),
              onPressed: signOut)
        ],
        leading: Builder(
          builder: (context) => IconButton(
            icon: new Icon(Icons.add),
            onPressed: () => Scaffold.of(context).openDrawer(),
            color: Colors.white,
          ),
        ),
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            ListTile(
              title: Text('Perfil'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Perfil()),
                );
              },
            ),
            ListTile(
                title: Text('Supermercados cercanos'),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Supermercado()),
                  );
                }),
            ListTile(
              title: Text('Calcula tus porciones'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Porciones()),
                );
              },
            ),
            ListTile(
                title: Text('Escríbenos'),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Escribenos()),
                  );
                }),
          ],
        ),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: FlatButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Mercado()),
                );
              },
              child: Image.asset(rutaImg + "planeamercado.png"),
              color: verde,
            ),
          ),
          Expanded(
            child: Row(
              children: <Widget>[
                Expanded(
                  child: FlatButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Familia()),
                      );
                    },
                    child: Image.asset(rutaImg + "familia.png"),
                    color: amarillo,
                  ),
                ),
                Expanded(
                  child: FlatButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Tips()),
                      );
                    },
                    child: Image.asset(rutaImg + "tips.png"),
                    color: naranja,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: FlatButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Recetas()),
                );
              },
              child: Image.asset(rutaImg + "recetas.png"),
              color: rojo,
            ),
          )
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_basket),
            title: Text('Mercado'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Inicio'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: Text('Perfil'),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Color.fromRGBO(242, 159, 5, 100),
        onTap: _onItemTapped,
      ),
    );
  }
}
