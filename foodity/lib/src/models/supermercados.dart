import 'package:flutter/material.dart';
import 'package:foodity/src/models/colors.dart';
import 'places_search_map.dart';


class Supermercado extends StatefulWidget {
  _Supermercado createState() => _Supermercado();
}

class _Supermercado extends State<Supermercado> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Supermercados cercanos', style: textS,),
        leading: IconButton(
          color: Colors.white,
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: PlacesSearchMapSample(),
    );
  }
}
