import 'package:flutter/material.dart';
import 'package:foodity/src/models/colors.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class Perfil extends StatefulWidget {
  _Perfil createState() => _Perfil();
}

class _Perfil extends State<Perfil> {
  static final databaseReference = Firestore.instance;
  var rating;

  _Perfil({Key key}) {
    this.rating = 200.0;
  }

  @override
  Widget build(BuildContext context) {
    rating = 200.0;
    return FutureBuilder(
      future: FirebaseAuth.instance.currentUser(),
      builder: (BuildContext context, snapshot) {
        var email = snapshot.data.email;
        return FutureBuilder(
          future:
              databaseReference.collection("usuarios").document(email).get(),
          builder: (BuildContext context, snapshot) {
            return Scaffold(
              appBar: AppBar(
                title: Text(
                  "Perfil",
                  style: textS,
                ),
              ),
              body: Column(
                children: <Widget>[
                  Card(
                    child: Column(children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(top: 20, bottom: 10),
                        child: Align(
                          child: new Container(
                              width: 150.0,
                              height: 150.0,
                              decoration: new BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: new DecorationImage(
                                      fit: BoxFit.fill,
                                      image: new NetworkImage(
                                          "https://i.imgur.com/BoN9kdC.png")))),
                        ),
                      ),
                      Container(
                        child: Text("María Camila González",
                            style: TextStyle(
                                fontSize: 22,
                                color: naranja,
                                fontWeight: FontWeight.w500)),
                      ),
                      Container(
                        child: Text("Bajar de peso",
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.grey,
                              fontWeight: FontWeight.w500,
                            )),
                      ),
                      Container(
                        child: Card(
                          color: naranja,
                          child: Row(
                            children: <Widget>[
                              Column(
                                children: <Widget>[
                                  Container(
                                    alignment: Alignment.center,
                                    child: Text(
                                      "Presupuesto",
                                      style: textS,
                                    ),
                                    padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    child: Text(
                                      rating.toString(),
                                      style: TextStyle(
                                          fontSize: 30, color: Colors.white),
                                    ),
                                    padding:
                                        EdgeInsets.fromLTRB(20, 10, 20, 10),
                                  ),
                                ],
                              ),
                              Container(
                                color: Colors.white,
                                width: 1,
                                height: 70,
                              ),
                              Column(
                                children: <Widget>[
                                  Container(
                                    alignment: Alignment.center,
                                    child: Text(
                                      "Familiares",
                                      style: textS,
                                    ),
                                    padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    child: Text(
                                      "1",
                                      style: TextStyle(
                                          fontSize: 30, color: Colors.white),
                                    ),
                                    padding:
                                        EdgeInsets.fromLTRB(20, 10, 20, 10),
                                  ),
                                ],
                              ),
                              Container(
                                color: Colors.white,
                                width: 1,
                                height: 70,
                              ),
                              Column(
                                children: <Widget>[
                                  Container(
                                    alignment: Alignment.center,
                                    child: Text(
                                      "Último plan",
                                      style: textS,
                                    ),
                                    padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    child: Text(
                                      "20 Sep 2019",
                                      style: TextStyle(
                                          fontSize: 16, color: Colors.white),
                                    ),
                                    padding:
                                        EdgeInsets.fromLTRB(20, 10, 20, 10),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        height: 100,
                        width: 400,
                      ),
                      Container(
                        child: Text(
                          "Tu familia:",
                          style: TextStyle(
                              fontSize: 20,
                              color: Colors.grey,
                              fontWeight: FontWeight.w400),
                        ),
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.only(left: 20, top: 20),
                      ),
                      Container(
                        height: 150,
                          child: Row(
                        children: <Widget>[
                          Container(
                              child: Column(
                            children: <Widget>[
                              Container(
                                child: Icon(
                                  Icons.person,
                                  size: 90,
                                  color: Colors.grey,
                                ),
                                width: 150,
                                height: 100,
                              ),
                              Container(
                                child: Text("Brayan",
                                    style: TextStyle(
                                        fontSize: 20, color: Colors.grey)),
                              ),
                            ],
                          )),
                          Container(
                            width: 150,
                            height: 250,
                            
                            child: Column(
                              children: <Widget>[
                                IconButton(
                                  icon: Icon(
                                    Icons.add_circle_outline,
                                    color: rojo,
                                    size: 90,
                                    
                                  ),
                                  onPressed: null,
                                ),
                              ],
                            ),
                          ),
                        ],
                      )), 
                      Container(height: 64,)
                    ]),
                  ), 
                  
                ],
              ),
            );
          },
        );
      },
    );
  }

  Widget showImg() {
    return new Hero(
      tag: 'hero',
      child: Padding(
        padding: EdgeInsets.fromLTRB(100.0, 10.0, 100.0, 5.0),
        child: Column(children: <Widget>[
          FlatButton(
            child: Image.asset(rutaImg + 'defaultImg.png'),
            onPressed: null,
          ),
          Text('Hola'),
        ]),
      ),
    );
  }

  Future<String> getDocument(String email) async {
    return await databaseReference
        .collection("usuarios")
        .document(email)
        .get()
        .then((snap) {
      return snap.data.toString();
    });
  }
}

class EditarPerfil extends StatefulWidget {
  _EditarPerfil createState() => _EditarPerfil();
}

class _EditarPerfil extends State<EditarPerfil> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(children: <Widget>[
      Text("Aquí va a ir editar perfil"),
      FlatButton(
        onPressed: () {
          Navigator.pop(context);
        },
        child: Text('Volver'),
      )
    ]));
  }
}
